package projectSyncPRL
import scala.collection.immutable.List

abstract class ACOparent {

  val dimension: Int = VRP.dimension
  val alpha: Double = 0.1
  val L2 = VRP.solCost
  val t0: Double = 1.0 / (L2 * (dimension - 1))

  def ACOalg()

  def chooseNext(k: Ant, pheroMatrix: Array[Array[WrapDouble]], hetaMatrix: List[List[Double]]): Int

  def twoOpt(rNodes: List[Int]): List[Int]
}
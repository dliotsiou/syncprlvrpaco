package projectSyncPRL

class WrapDouble (number:Double) {

  var value: Double = number

  def getValue: Double =
    return value

  def setValue(number: Double): Unit =
    this.value = number

}